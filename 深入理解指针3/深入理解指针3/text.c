﻿#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int Sub(int x, int y)
//{
//	return x - y;
//}
//
//int Mul(int x, int y)
//{
//	return x * y;
//}
//
//int Div(int x, int y)
//{
//	return x / y;
//}
//
//void Calc(int(*pf)(int, int))//函数指针
//{
//	int x, y;
//	int ret = 0;
//	printf("输入操作数:");
//	scanf("%d %d", &x, &y);
//	ret = pf(x, y);
//	printf("ret = %d\n", ret);
//
//}
//
//void menu()
//{
//	printf("****************************************\n");
//	printf("**********   1.Add     2.Sub   *********\n");
//	printf("**********   3.Mul     4.Div   *********\n");
//	printf("**********   0.exit            *********\n");
//	printf("****************************************\n");
//}
//
//int main() 
//{
//	int input = 0;
//	do
//	{
//		menu();
//		printf("请选择:");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			Calc(Add);
//			break;
//		case 2:
//			Calc(Sub);
//			break;
//		case 3:
//			Calc(Mul);
//			break;
//		case 4:
//			Calc(Div);
//			break;
//		case 0:
//			printf("退出计算器\n");
//			break;
//		default:
//			printf("选择错误,请重新选择\n");
//			break;
//		}
//	} while (input);
//	return 0;
//}
//1，相似的代码抽象成函数
//有了函数指针，可以使用函数名来调用
//也可以使用函数指针来调用


//qsort -- 用来排序
//库函数，直接可是用来排序数据
//底层使用的是快速排序的方法
//qsort函数可以排序任意类型的数据

//实现冒泡排序

//写一个冒泡排序的函数,对一组整形数据进行排序，排序为升序
//void bubble_sort(int arr[], int sz)
//{
//	//趟数
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < sz-1; i++)//一趟内部的两两比较
//	{
//		for (j = 0; j < j -1- i; j++)//决定内循环的趟数
//		{
//			int temp = 0;
//			if (arr[j] > arr[j + 1])
//			{
//				temp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = temp;
//			}
//		}
//	}
//
//}
//int main()
//{
//	int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
//	//排升序
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	bubble_sort(arr,sz);
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}

//                       //base 基础地址
//void qsort(void *base,//指针,指向的是待排序的数组的第一个元素    
//           size_t num, //是base指向的待排序数组的元素个数
//           size_t size    是base指向的待排序数组的元素的大小
//           int(*compar(const void*,const void*));//函数指针
//           //指向的就是两个元素的比较函数
//           );

//两个整形元素可以直接用>比较
//但是两个字符串，两个结构体元素是不能使用比较的

//把两个元素比较的方法，封装成函数
//然后把函数的地址传给排序函数


//void* 类型指针是无具体类型的指针,这种类型的指针不能直接解引用,也不能进行+-整数的运算
// 
//qsort 函数的实现者
//qsort 函数的使用者 
//#include<stdlib.h>
//void print_arr(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//printf("\n");
//}

//int cmp_int(const void* p1, const void* p2)
//{
//	if( *(int*)p1 > *(int*)p2 )
//	{
//		return 1;
//	}
//	else if(*(int*)p1 == *(int*)p2)
//	{
//		return 0;
//	}
//	else
//	{
//		return -1;
//	}
//}

//int cmp_int(const void* p1, const void* p2)
//{
//	return (*(int*)p1 - *(int*)p2);
//}
//
//void test1()
//{
//	int arr[] = { 9,8,7,6,5,4,3,2,1 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), cmp_int);
//	print_arr(arr, sz);
//}
//
//int main()
//{
//	test1();
//	return 0;
//}

//qsort排序一下结构体

//strcmp是按照着字符中的字符的ascll值来比较
//.和->都属于结构体成员访问操作符号
//.的用法，如果左边是结构体变量  .的右边是结构体成员名就可以直接访问
//结构体变量.结构体成员
//->结构体指针->成员名
//#include<stdlib.h>
//#include<string.h>
//struct Stu
//{
//	char name[20];//struct结构体
//	int age;
//
//};
////这里的两个结构体比较大小
////1.按照名字比较--字符串
////2.按照年龄比较--年龄
////两个字符串用strcmp
//int cmp_stu_by_name(const void* p1, const void* p2)
//{//void不能直接接引用,用强值类形转换
//	return strcmp(((struct Stu*)p1)->name, ((struct Stu*)p2)->name);
//}
////
//int cmp_stu_by_age(const void* p1, const void* p2)
//{
//	return ((struct Stu*)p1)->age - ((struct Stu*)p2)->age;
//}
//
//void test2()
//{
//	//在此函数当中又定义了一个以Stu为基础的数组
//	struct Stu arr[3] = { {"zhangsan", 20},{"lisi", 35},{"wangwu", 18} };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), cmp_stu_by_name);
//	qsort(arr, sz, sizeof(arr[0]), cmp_stu_by_age);
//}
//
//int main()
//{
//	test2();
//	return 0;
//}

//struct Stu
//{
//	char name[20];
//	int age;
//};
//
//void print(struct Stu* ps)
//{
//	//printf("%s %d\n", (*ps).name , (*ps).age);
//	printf("%s %d\n", ps->name, ps->age);
//}
//
////->结构体成员的间接访问操作符
////结构体指针->成员名
//int main()
//{
//	struct Stu s = { "zhangsan",18 };
//	//printf("%s %d\n", s.name, s.age);
//	print(&s);
//
//	return 0;
//}
//
//struct Stu
//{
//	char name;
//	int age;
//}s1, s2, s3;//s1,s2,s3是结构体变量
//
//typedef struct Stu
//{
//	char name[20];
//	int age;
//}stu;//stu是新的类型名(前提是前面又typedef)

//我们模仿qsort来实现一个冒泡排序的函数
//这个函数可以排序成任意类型的数据

//bubble_sort()//改造



//void Swap(char* buf1, char* buf2,size_t width)
//{
//	int i = 0;
//	for (i = 0; i < width; i++)
//		{
//		char tmp = *buf1;
//		*buf1 = *buf2;
//		*buf2 = tmp;
//		buf1++;
//		buf2++;
//		}
//}
//void bubble_sort(void* base, size_t sz, size_t width, int (*cmp)(const void* p1, const void* p2))
//{
	//趟数
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < sz-1; i++)//一趟内部的两两比较
//	{
//		for (j = 0; j < j -1- i; j++)//决定内循环的趟数
//		{
//			//比较arr[j] 和 arr[j+1]
//			if (cmp((char*)base + j * width, (char*)base + (j + 1) * width) > 0)
//			{
//				//交换两个元素
//				Swap((char*)base + j * width, (char*)base + (j + 1) * width);
//
//			}
//		}
//	}
//}

//void test3()
//{
////排序整形
////排序结构体
//	int arr[] = { 9,8,7,6,5,4,3,2,1 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), cmp_int);
//	print_arr(arr, sz);
//
//}
//int  main()
//{
//	test3();
//	return 0;
//}
//void* 的指针，是无具体类型的指针
//它的作用就是接受任何类型的地址


//1.sizeod(数组名)
//2.&数组名 
//int main()
//{
//	int a[] = { 1,2,3,4, };
//	printf("sizeof(a) = %zd\n", sizeof(a));//16
//	printf("sizeof(a + 0) = %zd\n", sizeof(a + 0));//a是首元素的地址-类型是int*,a+0 还是首元素的地址，是地址大小就是4/8
//	printf("sizeof(*a) = %zd\n", sizeof(*a));//a是首元素的地址,*a就是首元素,大小就是4个字节
//	//*a == a[0] == *(a+0)
//	printf("sizeof(a + 1) = %zd\n", sizeof(a + 1));//a是首元素的地址,类型是int*,a+1跳过1个整形，a+1就是的二个元素的地址
//	printf("sizeof(a[1]) = %zd\n", sizeof(a[1]));//a[1]就是第二个元素,大小就是4/8
//	printf("sizeof(&a) = %zd\n", sizeof(&a));//&a是数组的地址,数组的地址也是地址,是地址大小就是4/8个字节
//	printf("sizeof(*&a) = %zd\n", sizeof(*&a));//1.*&互相抵消，sizeof(*&a) == sizeof(a)
//	//2.&a 是数组的地址,类型是int(*)[4],对数组指针的解引用访问的是数组，计算的是数组的大小
//
//	printf("sizeof(&a + 1) = %zd\n", sizeof(&a + 1));//&a+1是跳过整个数组后的那个为位置的地址,是地址4/8个字节
//	printf("sizeof(&a[0]) = %zd\n", sizeof(&a[0]));//首元素的地址,大小4/8
//	printf("sizeof(&a[0] + 1) = %zd\n", sizeof(&a[0] + 1));//数组第二个元素的地址，大小是4/8个字节
//	return 0;
//}

//对于数组名 a--数组名
//a -  首元素的地址 int*         a+1
//&a   数组的地址   int(*)[4]   &a+1
//它们的区别无非就是类型不同，但它们的地址是相同大小的
//int*       a+1是跳过一个整形
//int(*)[4]  &a+1 就是跳过一个为整形数组指针,他跳过对应数组指针的大小，也就是一次性跳过整个是数组指针


//int main()
//{
//	char arr[] = { 'a','b','c','d','e','f' };
//
//	printf("sizeof(arr) = %d\n", sizeof(arr));//数组名单独放在sizeof内部了,计算的是数组的大小，单位是字节--6
//	printf("sizeof(arr + 0) = %d\n", sizeof(arr + 0));//arr是数组名表示首元素的地址,arr+0，是地址就是4/8字节
//	无论谁的地址它们所占空间的大小是一样的
//	printf("sizeof(*arr) = %d\n", sizeof(*arr));//arr是首元素的地址,*arr就是首元素,大小是1个字节
//	*arr = arr[0] = *(arr+0)
//	printf("sizeof(arr[1]) = %d\n", sizeof(arr[1]));//arr[1]是第二个元素，大小也是1个字节
//	printf("sizeof(&arr) = %d\n", sizeof(&arr));//&arr 是数组地址，数组的地址也是地址,大小是4/8个
//	&arr -- char (*)[6]
//	printf("(&arr + 1) = %d\n", sizeof(&arr + 1));//&arr+1,跳过整个数组,指向了数组后边的空间，4/8个字节//
//	printf(" sizeof(&arr[0] + 1) = %d\n", sizeof(&arr[0] + 1));//第二个元素的地址，是地址就是4/8个字节
//	return 0;
//}


//int main()
//{
//	char arr[] = { 'a','b','c','d','e','f' };
//	printf("strlen(arr) = %d\n", strlen(arr));//arr也是首元素的地址，数组中没有\0,就会导致越界访问，结果就是随机的
//	printf("strlen(arr + 0) = %d\n", strlen(arr + 0));//arr加0是首元素的地址数组中没有\0,就会导致越界访问，结果就是随机的
//	//printf("strlen(*arr) = %d\n", strlen(*arr));//arr是首元素的地址,*arr是首元素,就是'a','a'的ascii码值是97
//	//就相当于把97作为地址传递给了strlen,strlen得到的就是野指针，代码是错误的
//	//printf("strlen(arr[1]) = %d\n", strlen(arr[1]));// arr[1]--'b'--98,传给strlen函数也是错误的
//	printf("strlen(&arr) = %d\n", strlen(&arr));//&arr是数组的地址，起始位置是数组的第一个元素的位置，随机值x
//	printf("strlen(&arr + 1) = %d\n", strlen(&arr + 1));//&arr+1 随机值 x-6
//	printf("strlen(&arr[0] + 1) = %d\n", strlen(&arr[0] + 1));//从第二个元素开始向后统计的，得到的也是随机值x-1
//
//	return 0;
//}


//int main()
//{
//	char arr[] = "abcdef";
//	printf("%d\n", sizeof(arr));//计算的是数组总大小，是7个字节,包括'\0'
//	printf("%d\n", sizeof(arr + 0));//arr表示数组首元素的地址,arr+0还是首元素的地址，是地址就是4/8
//	printf("%d\n", sizeof(*arr));//arr表示数组首元素的地址，*arr就是首元素，大小是1字节
//	printf("%d\n", sizeof(arr[1]));//arr[1]是第二个元素,大小一个字节
//	printf("%d\n", sizeof(&arr));//数组的地址,是地址就是4/8
//	printf("%d\n", sizeof(&arr + 1));//数组的地址,+1跳过整个数组,还是地址，是地址就是4/8
//	printf("%d\n", sizeof(&arr[0] + 1));//arr[0]+1 第二个元素的地址，大小是4/8个字节
//	return 0;
//}

//int main()
//{
//	char arr[] = "abcdef";
//	printf("%d\n", strlen(arr));//6
//	printf("%d\n", strlen(arr + 0));//arr首元素的地址,arr[0]还是首元素的地址,向后再\0之前有6个字符
//	//printf("%d\n", strlen(*arr));//'a'--97 出错
//	//printf("%d\n", strlen(arr[1]));//'b'--98 出错
//	printf("%d\n", strlen(&arr));//&arr是数组的地址，也是从数组的第一个元素开始向后找 6
//	//&arr -- char (*)[7]
//	//size_t strlen(const char* s)
//	printf("%d\n", strlen(&arr + 1));//随机值
//	printf("%d\n", strlen(&arr[0] + 1));//5
//	return 0;
//}

//int main()
//{
//	const char* p = "abcdef";//使得p无法改变后面的指针变量
//	printf("%d\n", sizeof(p));//p是指针变量,我们计算的是指针变量的大小,4/8
//	printf("%d\n", sizeof(p + 1));//p+1 = b的地址,是地址大小就是4/8个字节
//	printf("%d\n", sizeof(*p));//p的类型是const char*,*p就是char类型,1个字节
//	printf("%d\n", sizeof(p[0]));//1.p[0]--> *(p+0)--> *p --> 'a' 大小是一个字节
//	//2.把常量字符串想象成数组 p可以理解为数组名 p[0]就是首元素
//
//	printf("%d\n", sizeof(&p));//取出的是p的地址，地址的大小是4/8个字节
//	printf("%d\n", sizeof(&p + 1));//&p+1 是跳过p指针变量后的地址,是地址就是4/8个字节
//	printf("%d\n", sizeof(&p[0] + 1));//&p[0]取出字符串首元素的地址，+1是第二个字符的地址，大小是4/8个字节
//
//	return 0;
//}


//int main()
//{
//	char* p = "abcdef";
//	printf("%d\n", strlen(p));//6
//	printf("%d\n", strlen(p + 1));//5
//	//printf("%d\n", strlen(*p));//*p就是a-'97',err
//	//printf("%d\n", strlen(p[0]));*p[0]--> *(p+0)-->*p err
//	printf("%d\n", strlen(&p));//取的是p本身的变量，和字符串"abcdef"没有关系
//	//从p这个指针变量的起始位置开始向后数的,p变量存放的地址是什么，不知道，所以答案是随机的
//	printf("%d\n", strlen(&p + 1));//随机值
//	//以上着两个变量没有关系
//	printf("%d\n", strlen(&p[0] + 1));//&p[0]-取出字符串字符的地址,+1是第二个字符的地址
//	//从第二个字符取出来的就是地址就是5
//	return 0;
//}


//二维数组
// 0 0 0 0
// 0 0 0 0 
// 0 0 0 0  3行4列
//int main()
//{
//	int a[3][4] = { 0 };
//	printf("%d\n", sizeof(a));//a是数组名，单独放在sizeof内部,计算的是数组的大小,单位是字节  - 43 -3*4*4字节
//	printf("%d\n", sizeof(a[0][0]));//a[0][0] 是第一行第一个元素 大小是4个字节
//	printf("%d\n", sizeof(a[0]));//a[0] 第一行的数组名，数组名单独放在sizeof内部了,计算的是数组的总大小 //4个字节
//	printf("%d\n", sizeof(a[0] + 1));//a[0]第一行的数组名 但是a[0]并没有单独放在sizeof内部，
//	//所以这里数组名就是首元素的地址,那就是arr[0][0]  加一后就是a[0][1]的地址，大小是4/8个字节
//
//	printf("%d\n", sizeof(*(a[0] + 1)));//(*(a[0] + 1)表示第一行第二个元素，大小就是4
//	printf("%d\n", sizeof(a + 1));//a作为数组名并没有单独放在sizeof内部，那a表示数组首元素的地址，
//	//是二维数组首元素的地址,也就是第一行地址,a+1,跳过一行，指向了第二行的地址,a+1是数组指针，是地址大小就是4/8个字节
//
//	printf("%d\n", sizeof(*(a + 1)));//1.a+1是第二行的地址,*(a+1)就是第二行，计算的是第二行的大小 - 16
//	//2. *(a + 1)==a[1],是第二行的数组名，sizeof(*(a + 1))就相当于sizeof(a[1]),意思是把第二行的数组名单独放在
//	//sizeof内部，计算的是第二行的大小
//	printf("%d\n", sizeof(&a[0] + 1));//是第一行的数组名，&a[0],取出的就是数组的地址，就是第一行的地址,
//	//&a[0]+1 就是第二行的地址,是地址大小就是4/8字节
//	printf("%d\n", sizeof(*(&a[0] + 1)));//*(&a[0]+1)意思是对第二行的地址解引用,访问的就是第二行，大小是16个字节
//	printf("%d\n", sizeof(*a));//a作为数组名并没有单独放在sizeof内部，那a表示数组首元素的地址，
//	//是二维数组首元素的地址,也就是第一行的地址,*a就是第一行，计算的就是第一行的大小 ，16字节
//	//*a == *(a+0) ==a[0]
//	printf("%d\n", sizeof(a[3]));//你不能说我站在银行门口就抢银行吧，虽然越界了但不影响访问
//	//a[3]无需真实存在,仅仅通过类型的推断就能算出长度
//	//a[3]是第四行的数组名,单独放在sizeof内部,计算的是第四行的大小 ，16个字节
//	return 0;
//}


//sizeof(int;)   实际上就是算他的类型，不会计算里面的内容
//sizeof(3+5)
// 
//*接应用就是算的是字节大小
//1.sizeod(数组名) 并且是在单独的情况下
//2.&数组名 


//int main()
//{
//	int a[5] = { 1, 2, 3, 4, 5 };
//	int* ptr = (int*)(&a + 1);
//	printf("%d,%d", *(a + 1), *(ptr - 1));
//	return 0;
//}



//在X86环境下
//假设结构体的⼤⼩是20个字节
//程序输出的结果是啥？
//struct Test
//{
//	int Num;
//	char* pcName;
//	short sDate;
//	char cha[2];
//	short sBa[4];
//}*p = (struct Test*)0x100000; //20按照16进制就是14
//
////指针加减整数
//int main()
//{
//	printf("%p\n", p + 0x1);
//	printf("%p\n", (unsigned long)p + 0x1);
//	printf("%p\n", (unsigned int*)p + 0x1);
//	return 0;
//}
//
//#include <stdio.h>
//int main()
//{
//	int a[3][2] = { (0, 1), (2, 3), (4, 5) };//()逗号表达式 应该用{}
//	//所以数组里面真正放的是 1 3 5 0 0
//	int* p;
//	p = a[0];//a[0]是，数组名表示首元素的地址，实际上就是a[0][0]的地址
//	//a[0]就是*(p+0)  - *p
//	printf("%d", p[0]);
//	return 0;
//}