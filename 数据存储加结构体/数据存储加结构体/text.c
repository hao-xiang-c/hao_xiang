﻿//#include <stdio.h>
//unsigned char i = 0;//无符号的char
//
////unigned char 的取消范围是0~255
//int main()
//{
//	for (i = 0; i <= 254; i++)//条件恒成立
//	{
//		printf("hello world\n");
//	}
//	return 0;
//}


//每次这种题，就想一下一个类型对应的圆
//#include <stdio.h>
//#include<windows.h>
//int main()
//{
//	unsigned int i = 0;//不能存负数
//	for (i = 9; i >=0; i--)//代码恒成立 
//	{
//		printf("%u\n", i);//无符号类型
//		Sleep(1000);
//	}
//	return 0;
//}

//#include <stdio.h>
////X86环境 ⼩端字节序
//int main()
//{
//	int a[4] = { 1, 2, 3, 4 };
//	int* ptr1 = (int*)(&a + 1);//&a -> int(*)4  
// 	int* ptr2 = (int*)((int)a + 1);//
//	printf("%x,%x", ptr1[-1], *ptr2);
//	//指针+1 ，取决于指针的类型
//	//整数+1 ，就是加1 
//	return 0;
//}


//小数在内存中的存储 
//#include <stdio.h>
//int main()
//{
//	int n = 9;//整形
//	float* pFloat = (float*)&n;
//	printf("n的值为：%d\n", n);
//	printf("*pFloat的值为：%f\n", *pFloat);
//	*pFloat = 9.0;
//	printf("num的值为：%d\n", n);
//	printf("*pFloat的值为：%f\n", *pFloat);
//	return 0;
//}
 //看问题的视角不同
//整数和浮点数在内存中的存储不同


//自定义类型
//c语言当中有内置类型(c语言本身有的类型)，也有自定义类型



//1.1.1 结构的声明 ?
//struct tag
//{
//	member - list;
//}variable - list;
//
//例如描述⼀个学⽣：
//struct Stu
//{
//	char name[20]; //名字
//	int age; //年龄
//	char sex[5]; //性别
//	char id[20]; //学号
//}; //分号不能丢
//
//1.1.2 结构体变量的创建和初始化 ?
//#include <stdio.h>
//struct Stu
//{
//	char name[20]; //名字
//	int age; //年龄
//	char sex[5]; //性别
//	char id[20]; //学号
//};
//int main()
//{
//	//按照结构体成员的顺序初始化
//	struct Stu s = { "张三", 20, "男", "20230818001" };
//	printf("name: %s\n", s.name);
//	printf("age : %d\n", s.age);
//	printf("sex : %s\n", s.sex);
//	printf("id : %s\n", s.id);
//	//按照指定的顺序初始化
//	struct Stu s2 = { .age = 18, .name = "lisi", .id = "20230818002", .sex =
//	"⼥" };
//	printf("name: %s\n", s2.name);
//	printf("age : %d\n", s2.age);
//	printf("sex : %s\n", s2.sex);
//	printf("id : %s\n", s2.id);
//	return 0;
//}
//
//
//1.2 结构的特殊声明 ?
//在声明结构的时候，可以不完全的声明。
//⽐如：
////匿名结构体类型
//struct
//{
//	int a;
//	char b;
//	float c;
//}x;
//struct
//{
//	int a;
//	char b;
//	float c;
//}a[20], * p;
//
//上⾯的两个结构在声明的时候省略掉了结构体标签（tag）。 ?
//那么问题来了？
////在上⾯代码的基础上，下⾯的代码合法吗？
//p = &x;
//
//警告：
//编译器会把上⾯的两个声明当成完全不同的两个类型，所以是⾮法的。
//匿名的结构体类型，如果没有对结构体类型重命名的话，基本上只能使⽤⼀次。
//1.3 结构的⾃引⽤ ?
//在结构中包含⼀个类型为该结构本⾝的成员是否可以呢？
//⽐如，定义⼀个链表的节点：
//struct Node
//{
//	int data;
//	struct Node next;
//};
//
//上述代码正确吗？如果正确，那 sizeof(struct Node) 是多少？
//仔细分析，其实是不⾏的，因为⼀个结构体中再包含⼀个同类型的结构体变量，这样结构体变量的⼤
//⼩就会⽆穷的⼤，是不合理的。
//正确的⾃引⽤⽅式：
//struct Node 1
//{
//int data;
//struct Node* next;
//};
//在结构体⾃引⽤使⽤的过程中，夹杂了 typedef 对匿名结构体类型重命名，也容易引⼊问题，看看
//下⾯的代码，可⾏吗？
//typedef struct
//{
//	int data;
//	Node* next;
//}Node;
//答案是不⾏的，因为Node是对前⾯的匿名结构体类型的重命名产⽣的，但是在匿名结构体内部提前使
//⽤Node类型来创建成员变量，这是不⾏的。 ?
//解决⽅案如下：定义结构体不要使⽤匿名结构体了
//typedef struct Node
//{
//	int data;
//	struct Node* next;
//}Node;


//存储加结构体