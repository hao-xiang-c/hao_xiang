//struct结构体
#include<stdio.h>


//struct EmptyStruct
//{
//	//空结构体
//};

//typedef struct Student Student;

typedef struct Birthday
{
	int year;//年份
	int month;//月份
	int day;//日
}Birthday;

typedef struct Student {//这里的Student加不加都可以
	int id;//学号
	char *name;
	int age;//年龄
	float score;
	Birthday birthday;
}Student, stu3;//stu3就是个变量
 
//打印学生结构体信息
//待打印的学生信息
void printStudentInfo(Student *pstu , int len)//传一个长度
{
	/*printf("学号:%d\t 姓名:%s\t 年龄:%d\t 成绩:%.2f\t 生日:%d-%d-%d\n",
		pstu->id, pstu->name, pstu->age, pstu->score,
		pstu->birthday.year, pstu->birthday.month, pstu->birthday.day);*/

	//数组指针
	for (int i = 0; i < len; ++i)
	{
		printf("学号:%d\t 姓名:%s\t 年龄:%d\t 成绩:%.2f\t 生日:%d-%d-%d\n",
			(pstu+i)->id, (pstu+i)->name, (pstu+i)->age, (pstu+i)->score,
			(pstu+i)->birthday.year, (pstu+i)->birthday.month, (pstu+i)->birthday.day);
		//或者是pStu[i]这种形式
		//啊脑汁根本转不过来
	}
}

int main()
{
	//数据类型加上变量名
	 //Student stu1;
	 ////stu1.birthday.day
	 ////多层的去访问
	 //Student stu2;   直接赋值
	//Student stu1 = { 1001,"郝翔",18,100,{2021,5,27} };
	//Student stu2 = { 1002,"虾仁",25,100,{2023,2,23} };
//  类型   变量名      初始化
	//Student stu3 = { .age = 100,.id = "刘德华" }; //貌似也可以
	//Student stu3 = {stu3.age=100}; 貌似也可以 并且里面也可以嵌套其他的结构体变量
	 
//访问成员使用.操作符
	///*printf("学号:%d\t 姓名:%s\t 年龄:%d\t 成绩:%.2f\t 生日:%d-%d-%d\n",
	//	stu1.id, stu1.name, stu1.age, stu1.score,
	//	stu1.birthday.year,stu1.birthday.month, stu1.birthday.day);

	//printf("学号:%d\t 姓名:%s\t 年龄:%d\t 成绩:%.2f\t 生日:%d-%d-%d\n",
	//	stu2.id, stu2.name, stu2.age, stu2.score,
	//	stu2.birthday.year, stu2.birthday.month, stu2.birthday.day);*/

	//结构体数组
	Student students[] = {
		{ 1001,"郝翔",18,100,{2021,5,27} },
		{ 1002,"虾仁",25,100,{2023,2,23} },
	};
	int len = sizeof(students) / sizeof(students[0]);
	printStudentInfo(students , len);
	

	//Student* pStu = &stu1;//

	//printStudent(pStu);//传进去，打印学生1;
	////printStudent(stu2);
	// pStu = &stu2;//重新指向stu2
	//printStudent(pStu);


	//传递结构体指针

	//结构体内存
	/*printf("%zd ", sizeof(Birthday));
	printf("%zd ", sizeof(Student));*/


	return 0;
}