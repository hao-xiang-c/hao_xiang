//#include<stdio.h>
//int main()
//{
//	int i = 69686;
//	printf("%p\n", i);//指针的地址
//	return  0;
//}
//例如:本质上的32位/64位传输线是将内存中的数字放到cpu当中
//而在传输线当中最多可以表示的是2的32次方个数
//而在这个范围的数可以由cpu将对应数值转到其对应二进制，再经传输线放到内存当中
//而可以理解为这些数它所对应的16进制地址是固定的
//但每次执行程序所分配的内存地址是不同的
//而所有的地址是由电脑的厂家写好并固定的(按照一个个的字节来排序并编号)

//#include<stdio.h>
//int main()
//{
//	int a = 20;
//	//printf("%p\n", &a);
//	int *p = &a;//p是指针变量—存放指针的变量—就是存放地址的
//    //int 是在说明p是指向的对象的int类型的,*是指明p是指针变量
//
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	char ch = 'w';
//	char *pc = &ch;
//	//在指针的眼里什么都是其传过来的对应地址
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	int a = 20;
//	int* p = &a;
//p也是变量,也得向内存申请一块空间
// 
//	*p = 100;//解引用操作(间接访问操作) *p等价于a
//	//*--解引用操作符
//
//	printf("%d\n", a);
//	return 0;
//}
//指针变量需要多大空间,是取决于存放是什么
//存放的是地址，地址存放需要多大的空间，指针的大小就是多大
//32位机器上,地址就是32个0/1的二进制序列，存储起来需要32个比特位，也就是4个字节

//#include<stdio.h>
//int main()
//{
//	int a = 10;
//	int* pa = &a;
//	printf("%zd\n", sizeof(pa));
//	printf("%zd\n", sizeof(int *));
//	printf("%p\n", pa);
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	char ch = 'w';
//	char* pc = &ch;
//
//printf("%zd\n", sizeof(pc));
//printf("%zd\n", sizeof(char *));
////指针变量的大小和类型无关
//printf("%p\n", pc);
//return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int a = 0x11223344;
//	char* pa = &a;
//	*pa = 0;
//	//指针类型决定了指针进行解引用操作符的时候访问多大的空间
//	//int * 的指针解引用访问4个字节
//	//char *的指针解引用访问1个字节
//	//也就是说在指针变量的大小是与所对应的机器相关
//
//	return 0;
//}
//

//#include<stdio.h>
//int main()
//{
//	int a = 10;
//	int* pa = &a;
//
//	char* pc = &a;
//	printf("pa = %p\n", pa);
//	printf("pa + 1 = %p\n", pa + 1);
//
//	printf("pc = %p\n", pc);
//	printf("pc + 1 = %p\n", pc + 1);
//
//	return 0;
//}
//指针类型决定了指针的步长，就是向前/向后走多大的距离
//例如；
//type * p
//p+i 就是跳过type类型的数据
//相当于跳过了i *sizeof(type)个字节
//#include<stdio.h>
//int main()
//{
//
//
//	return 0; 
//}

//根据实际的需要，选择适当的指针才能达到效果
//#include<stdio.h>
//int main()
//{
//	int a = 10;
//	int* pa = &a;
//	//void* pc = &a;
//	//void * 可以接受任意类型的地址
//
//	return 0;
//}

//const修饰指针

//#include<stdio.h>
//int main()
//{
//	const int n = 10;
//	//const是常属性-不能被修改了
// const修饰了n之后，n不能被修改了,但是n的本质还是变量
// n是常变量
//	n = 0;
//	printf("%d\n", n);
//
//}
//
//int main()
//{
//	//int arr[10] = { 0 };
//	const int n = 10;//本质上还是变量，c++中const修饰的n就是常量
//	int arr[n] = { 0 };
//	return 0;
//}

//int main()
//{
//	const int n = 10;
//	int* p = &n;
//	*p = 20;
//	printf("%d\n", n);
//	return 0;
//
//}


//int n =0 ;
// int * p = &n
//1.p是指针变量，里面存放了其他变量地址
//2.p是指针变量，*p,是指针指向的对象
//3.p是指针变量，p也有自己的地址，&p


//const修饰指针变量有两中情况
//1.const 放在*的左边:限制的是*p,意思是不能通过p来改变p指向对象的内容，但是p本身是可以改变的，p可以
//2.const 放在*的右边:限制的是p,意思是不能修改p本身的值，但是p指向的内容是可以通过p来改变
//int main()
//{
//	const int n = 10;
//	int m = 100;
//	const int* p = &n;
//	
//	//p = &m;
//	p = &n;
//	return 0;
//}

//int main()
//{
//	const int n = 10;
//	int m = 100;
//	  int*const  p = &n;
//
//	*p = 20;
//	
//	printf("%d\n", p);
//	return 0;
//}

//int main()
//{
//	int n = 10;
//	const int* const p = &n;
//	* p = &n;
//    p = &n;
//	return 0;
//}
 
//指针的运算
//
//指针加减整数
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	//打印数组的内容
//	//下标0，1，2，3，4，5，6，7，8，9
//	int i = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d\n", arr[i]);
//	}
//	return 0;
//}

//int main()
//{
//	//数组在内存中是连续存放的
//	//随着数组下标的增长，地址是由低到高变化的
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//		int i = 0;
//		int sz = sizeof(arr) / sizeof(arr[0]);
//		int* p = &arr[0];
//			for (i = 0; i < sz; i++)
//			{
//				printf("%d ", *p);
//				p++;
//			}
//	return 0;
//}
//
// 指针加减的运算
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int i = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int* p = &arr[0];
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(p+i));
//	}
//	return 0;
//}

//指针减指针
//指针+1之后还是指针
//指针减指针之后得到的是元素的个数

//int main()
//{
//	int arr[10] = { 0 };
//	printf("%d\n", &arr[9] - &arr[0]);//指针-指针
//	printf("%d\n", &arr[0] - &arr[9]);//指针-指针
//	//指针减指针的绝对值是指针和指针之间的元素的个数
//    //前提条件是两个指针指向了同一块空间
//
//	char ch[6] = { 0 };
//	printf("%d\n", &arr[9] - &ch[4]);
//	//①它们直接在内存中空的距离并不确定
//	//②它们直接的整形并不相同
//	return 0;
//}

//strlen是一个库函数
//专门用来求字符串的长度
//strlen
//#include<string.h>
//int my_strlen(char * p)
//{
//	int count = 0;
//	while(*p != '\0')
//	{
//		count++;
//		p++;//p+整数
//	}
//	return count;
//}
//
//int main()
//{
//	char arr[] = "abcdef";//abcdef\0
//	//数组名其实是数组首元素的地址
//	int len = my_strlen(arr);
//	printf("%d\n", len);
//	return 0;
//}

//第二种方式
//#include<string.h>
//int my_strlen(char* p)
//{
//	char* p1 = p;
//	while (*p != '\0')
//	{
//		p++;
//	}
//	return p - p1;//指针减指针
//}
//
//int main()
//{
//	char arr[] = "abcdef";//abcdef\0
//	//数组名其实是数组首元素的地址
//	int len = my_strlen(arr);
//	printf("%d\n", len);
//	return 0;
//}
//
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int* p = &arr[0];
//	while (p < arr + sz)//指针的关系运算
//	{
//		printf("%d ", *p);
//		p++;
//	}
//	return 0;
//}


//野指针(指针指向的位置是不可知的)

//int main()
//{
//	int* p;//局部变量(局部变量不初始化的时候它是随机值
//	*p = 20;
//	printf("%d\n", *p);
//	return 0;
//}

//指针未初始化
//指针越界
//也会导致也指针的出现
//int main()
//{
//	int arr[10] = { 0 };
//	int* p = &arr[0];
//	int i = 0;
//	for (i = 0; i <= 11; i++)
//	{
//		*(p++) = 1;
//	}
//	return 0;
//}

//指针在函数调用的中的弊端
//int* text()
//{
//	int n = 100;//这里的n为局部变量
//	//一但出了函数的定义域内存就会回收这块空间
//	//回收后就不属于当前程序了
//	return &n;
//}
//在这块地址不属于程序后,在返回操作系统后，通过先前的地址找新的已经不合适了
//int main()
//{
//	int* p = test();
//	printf("%d\n", *p);
//	return 0;
//}



//int main()
//{
	//	/*int a = 10;
	//	int* pa = &a;*/
	//	//*pa = 20;
	//	int* p = NULL;//空指针(它的地址是0,0也是一个地址)
	//	//但这个地址无法使用访问
	//	*p = 20;//
	//	printf("%d\n", p);
	//	return 0;
	//}
//	int a = 10;
//	int* p = &a;
//	p = NULL;
//	if (p != NULL)
//	{
//		*p = 20;
//	}
//	printf("%d\n", a);
//}

//使用指针就得初始化并判断是否为空指针


//assert断言
// assert(p != NULL)
//验证p里面放的是不是空指针
// 
#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
//#include<stdio.h>
//#include<assert.h>
//int main()
//{
//	//int a = 10;
//	//int* p = NULL;
//	//assert(p != NULL);assert(表达式)
//	//*p = 20;
//	//printf("%d\n", a);
//
//	int a = 0;
//	scanf("%d", &a);
//	assert(a != 0);
//	printf("%d\n", a);
//	return 0;
//}
// 
//#define NDEBUG
//#include<stdio.h>
//#include<assert.h>
//int main()
//{
//	int a = 10;
//	int* p = &a;
//	assert(p != NULL);
//	*p = 20;
//	printf("%d\n", a);
//	return 0;
//}

//指针的传值和调用
#include<string.h>
#include<assert.h>//只是通过s只是遍历了一下
//不希望通过s将arr的内容改掉
//加上const修饰的代码*s的内容就不会被修改了
//size_t人家创造的一个无符号类型
//size_t my_strlen(const char* s)//如果传的是空指针,s就是空指针
//{
//	size_t count = 0;
//	assert(s != NULL);
//	while (*s != '\0')
//	{
//		count++;
//		s++;
//	}
//	return count;
//}
//
//int main()
//{
//	//srtlen()转专门求字符串的长度 -- 统计的是\n之前字符串的个数
//	char arr[] = "abcdef";
//	size_t len = my_strlen(arr);
//	printf("%zd\n", len);
//	return 0;
//}
//

//写一个函数,交换两个整形变量的值

//void Swap(int x,int y)//进入这个空间x和y都有自己独立的地址
//{
//	int z = 0;
//	z = x;
//	x = y;
//	y = z;
//
//}
////a和b是实参  x和y是形参
////当实参变量传给形参变量的时候，形参是实参的一份临时拷贝
////实参的值拷贝一份到形参里面,形参有独立自己的空间 
////对于形参的修改不会影响到实参
//
//int main()
//{
//	int a = 10;
//	int b = 20;
//	printf("交换前:a=%d b=%d\n", a, b);
//	Swap(a, b);
//	printf("交换后:a=%d b=%d\n", a, b);
//	return 0;
//}
//将变量本身传过去了,这种叫传值调用
//int main()
//{
//	int a = 10;
//	a = 20;//1
//
//	int* p = &a;
//	*p = 20;//2
//
//	return 0;
//}

//void Swap2(int* pa, int* pb)//借助指针交换
//{
//	int z = 0;
//	z = *pa;//z=a
//	*pa = *pb;//a=b
//	*pb = z;//b=z
//}
//这种叫传(地)址调用
//int main()
//{
//	int a = 10;
//	int b = 20;
//	printf("交换前:a=%d b=%d\n", a, b);
//	Swap2(&a, &b);//创捷一个指针
//	printf("交换后:a=%d b=%d\n", a, b);
//	return 0;
//}

//数组名的理解
// 
//数组名确实是首元素的地址,但有两个列外
//sizeof(数组名),这里的数组名表示整个数组,计算的是整个数组的大小,单位是字节
//&数组名,这里的整个数组名也表示整个数组，取出的是整个数组的地址
//
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	printf("arr     = %p\n", arr);
//	printf("arr+1     = %p\n", arr+1);//跳过4个字节
//
//	printf("&arr[0] = %p\n", &arr[0]);
//	printf("&arr[0]+1 = %p\n", &arr[0] + 1);//跳过4个字节
//
//	printf("&arr = %p\n", &arr);
//	printf("&arr+1 = %p\n", &arr + 1);//跳过40个字节
//	//跳过整个数组
//
////	printf("%d\n", sizeof(arr));
//
//	return 0;
//}

//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	//打印数组的内容
//	int* p = arr; //int* p = &arr[0];
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int i = 0;
//	//给数组输入10个值
//	/*for (i = 0; i < sz; i++)
//	{
//		scanf("%d", &arr[i]);
//		printf("%d\n", arr[i]);
//	}*/
//	//输入
//	for (i = 0; i < sz; i++)
//	{
//		scanf("%d", p + i);
//	}
//    //输出
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(p+i));
//		p++;
//	}
//	return 0;
//}

//1.一维数组传参的时候,传过去的是数组首元素的地址
//2.形参的部分可以写成指针的形式,也可以写成数组的形式,但本质上店都是指针
//写成数组的形式是为了方便理解
//void test(int arr[])//本质上arr是指针
//{
//	int sz2 = sizeof(arr) / sizeof(arr[0]);
//	printf("sz2 = %d\n", sz2);//1
//}
//
//int main()
//{
//	int arr[10] = { 0 };
//	int sz1 = sizeof(arr) / sizeof(arr[0]);//10
//	printf("sz1 = %d\n", sz1);
//
//	test(arr);//这里的arr没有单独放在sizeof也没有取地址
//	//所以arr是数组首元素的地址
//	return 0;
//}

//排序  值的形式
//void bubble_sort(int arr[],int sz)
//{
//	int i = 0;
//	//趟数
//	for (i = 0; i < sz - 1; i++)
//	{
//		//一趟冒泡排序
//		int j = 0;
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				int tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//			}
//		}
//	}
//}
//
//void print_arr(int arr[],int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//
//}
//
//int main()
//{
//	int arr[] = { 9,8,7,6,5,4,3,2,1 };//降序
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	print_arr(arr, sz);
//	bubble_sort(arr, sz);
//	print_arr(arr, sz);
//	return 0;
//}
//int count = 0;
//void bubble_sort(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		int flag = 1;
//		int j = 0;
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//			count++;
//			if (*(arr + j) > *(arr + j + 1))
//			{
//				int tmp = *(arr + j);
//				*(arr + j) = *(arr + j + 1);
//				*(arr + j + 1) = tmp;
//				flag = 0;//不是有序
//			}
//		}
//		if(flag == 1)
//		{
//			break;
//		}
//	}
//}
//
//void print_arr(int arr[],int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//
//}
//
//int main()
//{
//	int arr[] = {0,1,2,3,4,5,6,7,8,9 };//降序
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	print_arr(arr, sz);
//	bubble_sort(arr, sz);
//	print_arr(arr, sz);
//	printf("count = %d\n", count);
//	return 0;
//}


//二级指针指针变量的地址
int main()
{
	int a = 10;

	int* p = &a;

	int** pp =&p;

	**pp = 20;
	printf("%d\n", a);
	return 0;
}