//#pragma once
#ifndef _SEQLIST_H_
#define _SEQLIST_H_

#include <stdio.h>
#include<string.h>

#pragma once//防止被重复的包含

#define MAX_SIZE 10//宏
typedef int SQDataType;  //数据类型

typedef struct SeqList //这个就是一个定义seqList的结构体名称罢了
{
	SQDataType a[MAX_SIZE];
	int size;
}SL;//这里才是真正的受影响的变量


//typedef struct SeqList SL;
//增删查改等接口函数
void  SeqListInit(SL* ps);

void SeqListPushBack(SL* ps, SQDataType x);
void SeqListPushFront(SL* ps, SQDataType x);


#endif