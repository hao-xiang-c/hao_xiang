//时间复杂度
//O(N)随着字符串的变长，线性也随着变长
//O(1)意味着我的效率是不变的
//不是说一层循环就是0(N) 量层循环就是0(N*2)
//看具体的程序

//算法的复杂度计算，喜欢省略简写成为logN
//因为很多地方不好求底数

//常见的时间复杂度
//O(N*2)
//0(N)
//O(logN)
//O(1)

//时间复杂度算个数
//空间复杂度算变量的个数


//题目:
// 数组nums包含从0到n的所有整数,但其中缺了一个,请在代码找出那个缺失的整数
//int missingNumber(int* nums, int numsSize)
//{
//	int x = 0;
//	for (int i = 0; i < numsSize; i++)
//	{
//		x ^= nums[i]; 
//	}
//	for (int j = 0; j < numsSize+1; ++j)
//	{
//		x ^= j;
//	}
//	return x;
//}
//
////给定一个数组，将数组中的元素向右移动k个位置，其中k是非负数
////要求空间复杂度为O(1)
//void rotate(int* num, int numsSize, int k)
//{
//	int tmp = nums[numsSize_1];
//	for (int end = numsSize - 2)
//	
//}



//顺序表

////#pragma once
//#ifndef _SEQLIST_H_
//#define _SEQLIST_H_
//
//#define N 10
//typedef int SQDataType;
//
//struct SeqList
//{
//	SQDataType a[N];
//	int size;
//}SL;
//
////typedef struct SeqList SL;
////增删查改等接口函
//void SeqListInit(SL s1);
//
//#endif



//顺序表的实现--静态分配
//#include<stdio.h>
//#define MaxSize 10
//typedef struct {
//	int data[MaxSize];//静态的数组
//	int length;
//}SqList;//顺序表
//
//void InitList(SqList& L)
//{
//
//}
//
//
//int main()
//{
//	SqList L;//声明一个顺序表
//	InitList(L);
//	return 0;
//}



#include "SeqList.h"
void  TestSeqlist1()
{   
	SL sl;
	SeqListInit(&sl);
}
int main()
{
	TestSeqlist1();

	return 0;
}