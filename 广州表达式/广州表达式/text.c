
#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
int main()
{
	//const char str[20] = "hello\n";
	//printf("%s", str);
	////在前面加上const后就不能修改字符串的元素了
	////str[0] = 'H';
	//printf("%s", str);
	////printf("HelloWorld\n");

	//const char* pStr = "hello\n";
	//printf("%s", pStr);
	// char* const pStr = "hello\n";
	// pStr[0] = 'J';
	////pStr[0] = 'H';
	//printf("%s", pStr);

	//strlen
	//size_t strlen(const char* str);
	//size_t类型表示字符串的长度

	//char * strcat(char * destination,const char *source)
                 //目标字符串首地址  源字符串首地址

	char dest[9] = "ILove";
	char src[4] = "You";

	printf("%s\n", src);
	printf("%s\n", dest);

	strcat(dest, src);

	printf("%s\n", src);
	printf("%s\n", dest);

	char* strcpy(char* destination, const char* source);
	return 0;
}