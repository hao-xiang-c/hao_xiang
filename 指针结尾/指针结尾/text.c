﻿#define _CRT_SECURE_NO_WARNINGS 
//假设环境是x86环境，程序输出的结果是啥？
//#include <stdio.h>
//int main()
//{
//	int a[5][5];
//
//	int(*p)[4];//p是一个数组指针，p指向的数组是4个整形元素
//
//	p = a;//虽然将a的值赋个了p，但是p的类型没有变,依旧是 int *(p)[4] 加一的话就是向后跳过4
//	//(但是所有的值都在a int *(a)[5]里面来进行)
//	return 0;	printf("%p,%d\n", &p[4][2] - &a[4][2], &p[4][2] - &a[4][2]);
//}
//指针-指针 绝对值得到是指针和指针之间的元素的个数

//#include <stdio.h>
//int main()
//{
//	int aa[2][5] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
//	int* ptr1 = (int*)(&aa + 1);
//	int* ptr2 = (int*)(*(aa + 1));
//	printf("%d,%d", *(ptr1 - 1), *(ptr2 - 1));
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	char* a[] = { "work","at","alibaba" };
//	//a 是指针数组 
//	char** pa = a;//存放第一个元素的地址
//	pa++;
//	printf("%s\n", *pa);
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	char* c[] = { "ENTER","NEW","POINT","FIRST" };//enter new point flrst 
//	//char *
//	char** cp[] = { c + 3,c + 2,c + 1,c };
//	char*** cpp = cp;
//	printf("%s\n", **++cpp);
//	printf("%s\n", *-- * ++cpp + 3); //-- 是他本身减减  将c+1改成了c 
//	printf("%s\n", *cpp[-2] + 3);
//	printf("%s\n", cpp[-1][-1] + 1);
//	return 0; 
//}
//除了++ 或 -- cpp的值改变了 但其他表达式的值并不会改变cpp本身的值


//加法的优先级是最低的
// 

//字符函数和字符串函数

//1.字符分类函数
#include<stdio.h>
#include<ctype.h>//
//int main()
//{
//	//int ret = islower('A');//判断是否为非0的一个值，返回一个2
//    //大写则返回一个0
//
//	//int ret = isspace('	');//判断是否为空白字符
//
//	printf("%d\n", ret);
//	return 0;
//}


//写一个代码，将字符串中的小写字母转大写，其他字符不变
//a - 97 A - 65 = 32
//b - 98 B - 66 = 32
//int main()
//{
//	char arr[] = "I Am a Student";
//	//            I Am a Student\0
//	//            0123
//	int i = 0;
//	while (arr[i] != '\0')
//	{
//		//
//		// if (arr[i] >= 'a' && arr[i] <= 'z')
//		if(islower(arr[i]))//大小写转化的函数
//		{
//			arr[i] -= 32;
//		}		
//		i++;
//	}
//	printf("%s\n", arr);
//	return 0;
//}

//int main()
//{
//	printf("%c\n", toupper('a'));//将小写字母打印成大写字母     
//	printf("%c\n", toupper('B'));
//
//	printf("%c\n", tolower('a'));
//	printf("%c\n", toupper('B'));//大写转小写
//
//	return 0;
//}


//int main()
//{
//	char arr[] = "I Am a Student";
//	            I Am a Student\0
//	            0123
//	int i = 0;
//	while (arr[i] != '\0')
//	{
//		
//		 if (arr[i] >= 'a' && arr[i] <= 'z')
//		if(islower(arr[i]))//大小写转化的函数  //isupper(判断小写)
//		{
//			arr[i] = toupper(arr[i]);//全大写   //tolower(全小写1)
//		}		
//		i++;
//	}
//	printf("%s\n", arr);
//	return 0;
//}

#include<string.h>
//int main()
//{
//	// 都是size-t类型的,除非强制类型转换  一定注意size_t
//	if ((int)strlen("abc") - (int)strlen("abcdef") > 0)
//	{
//		printf(">\n");
//	}
//	else
//	{
//		printf("<=\n");
//	}
//	return 0;
//}

//实现strlen函数三种情况的实现

//C/C++ 中的 assert 是一个宏，用于在运行时检查一个条件是否为真 
//如果条件不满足，则运行时将终止程序的执行并输出一条错误信息

//计数器版本
#include<assert.h>
//size_t my_strlen(char* s)
//{
//	assert(s);//检查
//	int count = 0;
//	while (*s != '\0')
//	{
//		count++;
//		s++;
//	}
//	return count;
//}
//int main()
//{
//	char arr[] = "abcdef";
//	size_t len = my_strlen(arr);
//	printf("%zd\n", len);
//	return 0;
//}
 
 
//不创建临时变量,求字符串的长度  -- 递归
//my_strlen("abcdef")
// -->1 + my_strlrn("bcdef")
//1 + 1 + my_strlen("cdef")
//size_t my_strlen(const char* s)
//{
//	if (*s == '\0')
//		return 0;
//	else
//		return 1 + my_strlen(s + 1);//s里面存的是首元素的地址的话,s+1才是下一个字符地址
//		
//}
//int main()
//{
//	char arr[] = "abcdef";
//	size_t len = my_strlen(arr);
//	printf("%zd\n", len);
//	return 0;
//}


//strcmp  strcpy strcat
//strncmp strncpy strncat 对应的函数


//string copy -- 字符串拷贝

//void my_strcpy(char* dest, const char* src)
//{
//	assert(src != NULL);
//	assert(dest != NULL);
//
//	//拷贝'\0'前面的内容
//	while (*dest++ = *src++)//*src != '\0'
//	{
//		
//		//虽然++的优先级较高,但是它的效果是后置产生的
//		//dest++;
//		// //src++;
//	}
//	//*dest = *src;//拷贝'\0'
//}
//
//int main()
//{
//	//strcpy将原字符串拷贝
//	//源字符串中必须有'\0'
//	//同时'\0'也会拷贝过来
//	char arr1[] = "hello bit";
//    char arr2[20] = { 0 };//目标空间是一定能被改的
//
//	my_strcpy(arr2, arr1);//接收值 需要传的值
//
//	printf("%s\n", arr2);
//	return 0;
//}


//void my_strcpy(char* dest, const char* src)
//{
//	assert(src != NULL);
//	assert(dest != NULL);
//	char* ret = dest;//这一步实际上是保留desc原本的值 然后返回'src'里面的值,这样更精确一点
//	while (*dest++ = *src++)
//	{//遇到'\0'就退出循环,并且还能将'\0'的值保留到里面
//		;
//	}
//	return ret;
//}
//
//int main()
//{
//	char arr1[] = "hello bit";
//	char arr2[20] = "xxxxxxxxxxxxxxxxxxx";
//
//	my_strcpy(arr2, arr1);
//
//	printf("%s\n", arr2);
//	return 0;
//}



//strcat 函数用来连接字符串

//模拟实现
//函数返回的是目标空间的起始地址
//char * my_strcat(char* dest ,const char* src)
//{
//	assert(dest && src);
//	char* ret = dest;
//     //1.找到目标空间的'\0'
//	while (*dest != '\0')
//		dest++;
//	//2.拷贝
//	while (*dest++ = *src++)
//	{
//		;//空语句
//	}
//	return ret;
//}
//int main()
//{
//	char arr1[20] = "hello ";//直接空间足够
//	char arr2[] = "world";//目标空间可修改
//
//	char* s = my_strcat(arr1, arr2);
//	printf("%s\n", arr1);
//	printf("%s\n", s);
//	//printf("%s\n", my_strcat(arr1, arr2));
//
//	return 0;
//}


//int main()
//{
//	char arr1[] = "abcdef";
//	char arr2[20] = { 0 };
//	//arr2 = arr1;//这样写不行 因为是首元素的地址，他是起始地址,但是并不代表那块空间
//	strcpy(arr2, arr1);
//	printf("%s\n", arr2);
//	return 0;
//}

//char* my_strcat(char* dest,const char*src)
//{
//	assert(dest && src);
//	char* ret = dest;
//	//1.找到目标空间中的'\0'
//	while (*dest != '\0')
//	{
//		*dest++;
//	}
//	//2.数据的拷贝     
//	while (*dest++ = *src++)
//	{
//		;
//	}
//	return ret;
//}
////这个代码
////1.死循环
////2.非法越界访问,程序奔溃
//int main()
//{
//	char arr1[20] = "abcdef";
//	//char arr2[20] = "ghi";
//
//	my_strcat(arr1, arr1);
//	printf("%s\n", arr1);
//	return 0;
//}

//char* my_strcat(char* dest, const char* src)
//{
//	assert(dest && src);
//	char* ret = dest;
//	while (*dest != '\0')
//	{
//		*dest++;
//	} 
//	while (*dest++ = *src++)
//	{
//		;
//	}
//	return ret;
//}
//int main()
//{
//	char arr1[20] = "abcdef";
//	//char arr2[20] = "ghi";
//
//	my_strcat(arr1, arr1);
//	printf("%s\n", arr1);
//	return 0;
//}

//两个字符串的比较
//strcmp
//int main()
//{
//	//char arr1[] = "abcdef";
//	//char arr2[] = "abcdef";
//	////if (arr1 == arr2)//比较的是地址
//	////{
//
//	////}
//	//char* p = "abcdef";//首字符的地址
//	//if ("abcdef" == "abc")//也是在比较地址  比较的也是两个地址
//	//{
//
//	//}
//
//	//如果要比较两个字符串的内容，需要strcmp
//
//	return 0;
//}
//
//int main()
//{
//	char arr1[] = "abcdef";
//	char arr2[] = "abq";
//	int ret = strcmp(arr1, arr2);//值1比值2小的时候返回小于1的数
//	//printf("%d\n", ret); 
//	//if(ret>0)//判断返回值
//	return 0;
//

//模拟实现 ①
//int my_strcmp(const char* str1, const char* str2)
//{
//	assert(*str1 && *str2);
//	while(*str1 == *str2)
//	{
//		if (*str1 == '\0')
//			return 0;
//		str1++;
//		str2++;
//	}
//	if (*str1 > *str2)
//	{
//		return 1;
//	}
//	else
//	{
//		return -1;
//	}
//}

//模拟实现 ②
//int my_strcmp(const char* str1, const char* str2)
//{
//	assert(*str1 && *str2);
//	while (*str1 == *str2)
//	{
//		if (*str1 == '\0')
//			return 0;
//		str1++;
//		str2++;
//	}
//	return *str1 - *str2;//返回大于0或小于0的数字
//}
//int main()
//{
//	char arr1[] = "abcdef";
//	char arr2[] = "abq";
//	int ret = my_strcmp(arr1, arr2);
//	printf("%d\n", ret);
//	return 0;
//}

//strcpy(拷贝) strcat(连接) strcmp(比较)
//长度不受限制的字符串函数

//strncpy strncat strncmp(长度受限制的字符串函数)

//strncpy(有长度拷贝)
//int main()
//{
//	char arr1[] = "abcdef";
//	char arr2[20] = { 0 };
//	strncpy(arr2, arr1, 3);//要求几个弄几个 就算不够也要补齐'\0'
//	printf("%s\n", arr2);
//	return 0;
//}

//strncst(有长度连接)
//int main()
//{
//	char arr1[] = "abcdef";
//	char arr2[20] = "xx\0xxxxxxxxxx";//追加过程中会放'\0'
//	int len = strlen(arr1);
//	strncat(arr2, arr1, len);
//
//	printf("%s\n", arr2);//xxabc
//	return 0;
//}

//strncmp
//int main()
//{
//	char arr1[] = "abcdef";
//	char arr2[] = "abqdefghi";
//	int ret = strncmp(arr1, arr2, 6);//最多比较6个，大前面出结果后,就不比较了
//	printf("%d\n", ret);
//	return 0;
//}

//int main()
//{
//	char arr1[10] = "abcdef";
//	char arr2[] = "abqdefghiqqq";
//	strcpy(arr1, arr2);//不管内存够不够，就是拷贝(将之前的覆盖了)
//	printf("%s\n", arr1);
//	return 0;
//}

//int main()
//{
//	char arr1[10] = "abcdef";
//	char arr2[] = "abqdefghiqqq";
//	strcpy(arr1, arr2);
//	printf("%s\n", arr1);
//	return 0;
//}


//strstr的使用和模拟实现
//
//int main()
//{
//	char arr1[] = "this is an apple";
//	const char* p = "Apple";//没有的话就打印空指针
//	char* ret = strstr(arr1, p);//判断的是arr1里面有没有出现p里面的字符串
//	printf("%s\n", ret);//返回的是is 所以打印is is an apple
//	if (ret != NULL)
//	{
//		printf("%s\n", ret);
//	}
//	else
//	{
//		printf("找不到\n");
//	}
//	return 0;
//}

//模拟实现strstr函数
//1.专门得有一个指针变量,记录开始匹配的值                           
//2.可能存在多次匹配
//char* my_strstr(const char* str1, const char* str2)
//{
//	const char* s1 = NULL;
//	const char* s2 = NULL;
//	const char* cur = str1;
//
//	if (*str2 == '\0')//特殊情况下，假设arr2传的是一个空的字符串
//		return (char*)str1;
//
//	while (*cur)//当它等于'\0'就自动退出去了
//	{
//		s1 = cur;
//		s2 = str2;
//		while (*s1 != '\0' && *s2 != '\0' && *s1 == *s2)//不相等就跳出来了 这里首先是首元素的地址
//		{
//			s1++;
//			s2++;
//		}
//		if (*s2 == '\0')
//		{
//			return (char*)cur;
//		}
//		cur++;
//	}
//	return NULL;
//}
//
//int main()
//{
//	char arr1[] = "abcdefabcdef";
//	char arr2[] = "cdef";
//	char * ret = my_strstr(arr1, arr2);
//	//printf("%s\n", ret);
//	if (ret != NULL)
//	{
//		printf("%s\n", ret);
//	}
//	else
//	{
//		printf("找不到\n");
//	}
//	return 0;
//}
//


//strtok函数的使用 
//int main()
//{
//	char arr[] = "zpengwei@yeah.net@hehe";
//	char arr2[30] = { 0 };//"zpengwei\0yeah.net"
//	strcpy(arr2, arr);
//	const char* sep = "@.";//截断的条件 不讲究顺序
//	char* ret = NULL;
//	for (ret = strtok(arr, sep); ret != NULL; ret = strtok(NULL, sep))
//	{
//		printf("%s\n", ret);
//	}
//
//
//	//ret = strtok(arr2, sep);
//	//printf("%s\n", ret);
//
//	//ret = strtok(NULL, sep);
//	//printf("%s\n", ret);
//
//	//ret = strtok(NULL, sep);
//	//printf("%s\n", ret);
//
//	return 0;
//}


//strerror函数的使用

//int main()
//{
//	int i = 0;
//	for (i=0;i<=10;i++)
//	{
//		printf("%d:    %s\n",i, strerror(i));
//	}
//	return 0;
//}

//#include <errno.h>
//int main()
//{
//	//fopen以读的形式打开文件的时候,如果文件不存在，就打开失败
//	FILE *pf = fopen("test.txt","r");
//		if (pf == NULL)
//		{
//			printf("%s\n", strerror(errno));
//			return 1;
//		}
//       //读文件
//
//	  //关闭文件
//		fclose(pf);
//	
//	return 0;
//}

//#include <errno.h>
//int main()
//{
//	
//	FILE* pf = fopen("test.txt", "r");
//	if (pf == NULL)
//	{
//		perror("hehe");//perrer有能力直接打印错误信息的，打印的时候，先打印传给perrer的字符串
//		//然后打印冒号，再打印空格，最后打印错误码对应的错误信息
//		//perror == printf + strerror(errno)
//		return 1;
//	}
//	//读文件
//
//   //关闭文件
//	fclose(pf);
//
//	return 0;
//}

