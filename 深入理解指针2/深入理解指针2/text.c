//指针数组
#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
//类比
//整形数组 int arr[10];//存放整形的数组
//字符数组 char arr[5];//存放字符的数组
//指针数组——>存放指针的数组

//使用指针数组来模拟实现二维数组

//int main()
//{
//	//int* arr[10];//指针数组
//	int arr1[] = { 1,2,3,4,5 };
//	int arr2[] = { 2,3,4,5,6 };
//	int arr3[] = { 3,4,5,6,7 };
//
//	int* arr[3] = { arr1,arr2,arr3 };
//
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		int j = 0;
//		for (j = 0; j < 5; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}

//字符指针变量
//int main()
//{
//	char ch = 'w';
//	char* pc = &ch;
//	printf("%c\n", *pc);
//	*pc = 'q';
//	printf("%c\n", *pc);
//	return 0;
//}

//int main()
//{
//	const char* p = "hello world";//理解为字符数组
//	//p指向常量字符串是不能被接引用修改的
//	//char* p = arr;  数组是可以改变的
//	//*p = 'o';
//	printf("%c\n", *p);
//	return 0;
//}
//#include<string.h>
//int main()
//{
//	const char* p = "hello world";
//	//printf("%s\n", p);
//	//printf("%s\n", "hello world");
//	int len = strlen(p);
//	int i = 0;
//	for (i = 0; i < len; i++)
//	{
//		printf("%c", *(p + i));
//	}
//	return 0;
//}

//int main()
//{
//	int a = 20;
//	int* p = &a;
//	printf("%d\n", a);
//	printf("%d\n", p);
//	return 0;
//}

//int main()
//{
//	char arr[] = "abcdef";
//	char* p = arr;
//	printf("%s\n", arr);
//	printf("%s\n", p);
//	return 0;
//}
// 
//#include <stdio.h>

//int main()
//{
//	char str1[] = "hello bit.";
//	char str2[] = "hello bit.";
//	//str1和str2所指向的首地址完全是两个不同的元素地址
//	const char* str3 = "hello bit.";
//	const char* str4 = "hello bit.";
//	//而str3和str4是常量字符(常量字符是不可修改的)
//	//并且存一份就够了,所以它们指向的是同一块地址
//	if (str1 == str2)
//		printf("str1 and str2 are same\n");
//	else
//		printf("str1 and str2 are not same\n");
//
//	if (str3 == str4)
//		printf("str3 and str4 are same\n");
//	else
//		printf("str3 and str4 are not same\n");
//
//	return 0;
//}


//字符指针 char *p 指向字符的指针,存放的是字符的地址
//整形指针 int *p  指向整形的指针,存放的是整形的地址

//数组指针------指向数组的指针,存放的是数组的地址

//数组指针是一种指针变量, 是存放数组地址的指针变量
//int main()
//{
//	int arr[10];
//	arr;//数组是数组首元素的地址
//	&arr[0];//数组首元素的地址
//	&arr;//取出的是数组的地址
//	return 0;
//}

//int main()
//{
//	int arr[10] = { 0 };
//	int(*p)[10] = &arr;//取出的是数组的地址
//	//p应该是数组指针,p指向的是数组,数组10个元素,每个元素的类型是int
//
//	return 0;
//}

//int main()
//{
//	//int* ptr;
//	char arr[5] = { 0 };
//	char(*p)[5] = &arr;//p是数组指针
//	//char (*)[5]数组指针类型
//
//	/*char* arr[5] = { 0 };
//	char* (*p)[5] = &arr;*///p是数组指针
//	return 0;
//}

//arr (arr+1) 跳过4个字节
//&arr[0] &arr[0]+1 跳过4个字节
//&arr  &arr+1  跳过40个字节  (int (*p)[10] =&arr) p是数组指针
//      int (*)[10](指针类型决定)

//int arr1[5]; int [5]
//int arr2[8]; int [8]
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int* p = arr;
//	int i = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(p + i));//p(地址)
//	}
//	return 0;
//}

//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int(*p)[10] = &arr;
//
//	int i = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", (*p)[i]);//p(地址)
//	}
//	return 0;
//}

//数组指针,会在二维数组中使用

//void test(int arr[3][5], int r, int c)
//{
//	int i = 0;
//	for (i = 0; i < r; i++)
//	{
//		int j = 0;
//		for (j = 0; j < c; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//}
//
//int main()
//{
//	int arr[3][5] = { {1,2,3,4,5}, {2,3,4,5,6}, {3,4,5,6,7,} };
//	test(arr, 3, 5);
//	return 0;
//}
//数组名是数组首元素的地址
//二维数组名是谁的地址
//1 2 3 4 5
//2 3 4 5 6 
//3 4 5 6 7
//arr是二维数组
//数组名是首元素的地址
//二维数组的首元素就是第一行
//每一行都是一个元素(一维数组)


//二维数组的数组名就是第一行的地址
//第一行是一个一维数组

//int arr[3][5]
//数组指针--是指向一维数组的指针
//int (*p)[5]=arr
//void test(int (*p)[5], int r, int c)
//{
//	int i = 0;
//	for (i = 0; i < r; i++)
//	{
//		int j = 0;
//		for (j = 0; j < c; j++)
//		{
//			printf("%d ", (*(p + i))[j]);
//			//一维数组指向首元素的地址,那么二维数组只需要跳过一个整形，就来到了下一行的数组
//			//实际上二维数组就是由多个一维数组组成
//		}
//		printf("\n");
//	}
//}
//
//int main()
//{
//	int arr[3][5] = { {1,2,3,4,5}, {2,3,4,5,6}, {3,4,5,6,7,} };
//	test(arr, 3, 5);
//	return 0;
//}
//
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr[i]);//首元素的地址
//		//arr[i]--->*(arr+i)
//		//arr+i 产生的是下标为i元素的地址
//	return 0;
//}


//字符指针 ——>指向指针--存放字符的的地址
//整形指针 ——>指向整形--存放整形的地址
//数组指针 ——>指向数组--存放数组的地址
//函数指针 ——>指向的是函数--存放的是函数的地址

//int Add(int a, int b)
//{
//	return a + b;
//}
//
//int* test(char* s)
//{
//	return NULL;
//}
////函数指针变量的写法个数组指针变量的写法非常类似
//int main()
//{
//
//	int* (*pt)(char) = test;
//	//int (*pf)(int, int) = &Add;//pf 就是函数指针变量*()
//	int arr[8] = { 0 };
//	int(*pa)[8] = &arr;//pa是数组指针变量
//
//	int (*pf)(int, int) = &Add;//pf 就是函数指针变量
//	/*int x = 10;
//	int y = 20;
//	int z = Add(x, y);*/
//	/*printf("%p\n", &Add);
//	printf("%p\n", Add);*/
//
//    //&函数名和函数名都表示函数的地址
//	return 0;
//}


//int Add(int a, int b)
//{
//	return a + b;
//}
//
//int main()
//{
//	int (*pf1)(int, int) = &Add;//pf 就是函数指针变量
//	int (*pf2)(int, int) = Add;//函数名和函数都是函数的地址
//
//	int r1 = (*pf1)(3, 7);
//	int r2 = (*pf2)(5, 7);
//	int r4 = pf2(5, 7);//*就是个摆设
//
//	int r3 = Add(2, 3);
//
//	printf("%d ", r1);
//	printf("%d ", r2);
//	printf("%d ", r3);
//	printf("%d ", r4);
//
//
//	return 0;
//}


//函数声明
//函数调用
//void (*)()  函数指针类型
//int main()
//{
//
//	(*( void (*)() ) 0 )();//函数调用
//	//1.将0强制类型转化成void (*)()类型的函数指针
//	//2.调用0地址处放的这个函数
//	return 0;
//}


//函数声明
//声明的函数的名字叫 ：signal
//signal函数有2个参数,第一个函数的类型是int 
//第二个函数的类型是void(*)(int)的函数指针类型，该指针可以指向一个函数,指向的函数参数是int
//signal函数的返回类型是void(*)(int)的函数指针
//
//void (* signal(int, void(*)(int) ) )(int);//void 函数返回类型
////void(int)  (*signal(int, void(*)(int)));这么理解较为容易一些,但是不支持这种写法
//
//int main()
//{
//
//	return 0;
//}

//typedef unsigned int uint;//typedef将复杂的类型简单化
//int main()
//{
//	unsigned int num;
//	uint num2;
//
//	return 0;
//}

//typedef 对指针类型重命名

//typedef int* print;
//int main()
//{
//	int* p1 = NULL;
//	print p2 = NULL;
//	return 0;
//}

//
//typedef int(*parr_t)[5];//语法要求
////parr_t  等价于 int(*)[5]
////parr_t -->类型
////p --->变量名
//int main()
//{
//	int arr[5] = { 0 };
//	int (* p)[5] = &arr;//p是数组指针变量,p是变量的名字;
//	//int (*)[5] -->数组指针类型
//	parr_t p2 = &arr;//parr_t此时就是数组名类型-->等价于定义的int (*p)[5]
//
//	return 0;
//}


//void test(char* s)
//{
//
//}
//
////对函数指针类型重命名产生新的类型pf_t
//typedef void (*pf_t) (char*);
//
//int main()
//{
//	void (*pf) (char*) = test;//函数声明  函数调用
//	//pf类型---->void (*)(char *)-->函数指针类型
//	pf_t pf2= test;
//
//	return 0;
//}



//
//void (*signal(int, void(*)(int)))(int);
//
//
////简化后的代码
//typedef void (*pf_t)(int);
//pf_t signal2(int, pf_t);

//函数的形参名字在声明的时候可以省略 (在声明的时候压根就不会用)
//
//typedef int* ptr_t;//typedef 定义的更加完整一些
//
//#define PTR_T int*
//
//int main()
//{
//	//ptr_t p1;//p1是整形指针
//	//PTP_T p2;//p2是整形指针
//
//	ptr_t p1, p2;//p1,p2 都是整形指针
//	PTR_T p3, p4;//p3是整形指针  而p4是一个整形
//	//int *p3 , p4;
//	return 0;
//}


//函数指针 --- 指向函数的指针
//函数指针变量 - 存放函数地址的变量

//int Add(int x ,int y)
//{
//
//	return x + y;
//}
//int main()
//{ 
//	printf("%p\n", Add);
//	printf("%p\n", &Add);
//
//	int(*pf) (int, int) = Add;//pf就是函数的指针变量
//	//int ret = (*pf)(3, 4);  解引用
//	int ret = pf(3, 4);
//	//int ret = *pf(3, 4);  错误的写法 编译器会理解成为pf先解引用(3.4)，然后返回的值是7，就成了*7
//	//Add(3,4);
//	printf("%d\n", ret);
//	return 0;
//}

//函数指针数组
//类比
//整形数组：是存放指针的数组
//字符数组：是存放字符的数组

//指针数组：存放指针的数组
//char* arr1[5];//字符指针数组
//int* arr2[5];整形指针数组

//如果要把多个相同类型的函数指针存放在一个数组中就是函数指针数组

//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int Sub(int x, int y)
//{
//	return x - y;
//}
//
//int Mul(int x, int y)
//{
//	return x * y;
//}
//
//int Div(int x, int y)
//{
//	return x / y;
//}
//
//
//int main()
//{
//	int (*pf1)(int, int) = Add;
//	int (*pf2)(int, int) = Sub;
//	int (*pf3)(int, int) = Mul;
//	int (*pf4)(int, int) = Div;
//
//	int (*pfArr[4])(int, int) = { Add,Sub,Mul,Div };//pfArr就是函数指针数组
//	//后面的内容一定要初始化
//	int i = 0;
//	for (i = 0; i < 4; i++)
//	{
//		int ret = pfArr[i](8, 4);
//		printf("%d\n", ret);
//	}
//
//	return 0;
//}

//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int Sub(int x, int y)
//{
//	return x - y;
//}
//
//int Mul(int x, int y)
//{
//	return x * y;
//}
//
//int Div(int x, int y)
//{
//	return x / y;
//}
//
////想写一个计算器
////完成2个整数的运行
////1.加法
////2.减法
////3.乘法
////4.除法
////
//
//void menu()
//{
//	printf("****************************************\n");
//	printf("**********   1.Add     2.Sub   *********\n");
//	printf("**********   3.Mul     4.Div   *********\n");
//	printf("**********   0.exit            *********\n");
//	printf("****************************************\n");
//
//
//}
//int main() 
//{
//	int input = 0;
//	int x = 0;
//	int y = 0;
//	int ret = 0;
//	do
//	{
//		menu();
//		printf("请选择:");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			printf("请输入2个操作数:");
//			scanf("%d %d", &x, &y);
//			ret = Add(x, y);
//			printf("%d\n", ret);
//			break;
//		case 2:
//			printf("请输入2个操作数:");
//			scanf("%d %d", &x, &y);
//			ret = Sub(x, y);
//			printf("%d\n", ret);
//			break;
//		case 3:
//			printf("请输入2个操作数:");
//			scanf("%d %d", &x, &y);
//			ret = Mul(x, y);
//			printf("%d\n", ret);
//			break;
//		case 4:
//			printf("请输入2个操作数:");
//			scanf("%d %d", &x, &y);
//			ret = Div(x, y);
//			printf("%d\n", ret);
//			break;
//		case 0:
//			printf("退出计算器\n");
//			break;
//		default:
//			printf("选择错误,请重新选择\n");
//			break;
//		}
//	} while (input);
//	return 0;
//}


//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int Sub(int x, int y)
//{
//	return x - y;
//}
//
//int Mul(int x, int y)
//{
//	return x * y;
//}
//
//int Div(int x, int y)
//{
//	return x / y;
//}
//
//void menu()
//{
//	printf("****************************************\n");
//	printf("**********   1.Add     2.Sub   *********\n");
//	printf("**********   3.Mul     4.Div   *********\n");
//	printf("**********   0.exit            *********\n");
//	printf("****************************************\n");
//
//
//}
//int main()
//{
//	int input = 0;
//	int x = 0;
//	int y = 0;
//	int ret = 0;
//    //函数指针变量
//	//转移表
//	int (*pfArr[5])(int, int) = { NULL,Add,Sub,Mul,Div };
//	//                              1   2   3  4   5 
//	do
//	{
//		menu();
//		printf("请选择:");
//		scanf("%d", &input);
//		if (input >= 1 && input <= 4)
//		{
//			printf("请输入操作数:");
//			scanf("%d %d", &x, &y);
//			ret = pfArr[input](x, y);
//			printf("%d\n", ret);
//		}
//		else if (input == 0)
//		{
//			printf("退出游戏\n");
//			break;
//		}
//		else
//		{
//			printf("选择错误,重新选择\n");
//		}
//	} while (input);
//	return 0;
//}


//一级指针 --> char* p  int* p
 
//二级指针 --> char** pp = &p  int** p = &p
 
//数组指针 --> 指向的是数组  int arr[5] -- int (*p)[5] = &arr
 
//函数指针 --> 指向的是函数  char* text(int* a,char* b)  --  char* (* pf)(int* ,char*) = text//pf是函数指针

//指针数组 --> char* arrr[5]  int* arr2[5]  double* arr3[9]  float* arr4[6]

//函数指针数组 --> char* (* pfArr [4])(int,char*)


//
//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int Sub(int x, int y)
//{
//	return x - y;
//}
//
//int Mul(int x, int y)
//{
//	return x * y;
//}
//
//int Div(int x, int y)
//{
//	return x / y;
//}
//
//void menu()
//{
//	printf("****************************************\n");
//	printf("**********   1.Add     2.Sub   *********\n");
//	printf("**********   3.Mul     4.Div   *********\n");
//	printf("**********   0.exit            *********\n");
//	printf("****************************************\n");
//}
//
//void Calc(int (*pf)(int, int))
//{
//	
//	int x = 0;
//	int y = 0;
//	int ret = 0;
//	printf("请输入2个操作数:");
//	scanf("%d %d", &x, &y);
//	ret = pf(x, y);
//	printf("%d\n", ret);
//
//}
//int main() 
//{
//	int input = 0;
//	do
//	{
//		menu();
//		printf("请选择:");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			Calc(Add);
//			break;
//		case 2:
//			Calc(Sub);
//			break;
//		case 3:
//			Calc(Mul);
//			break;
//		case 4:
//			Calc(Div);
//			break;
//		case 0:
//			printf("退出计算器\n");
//			break;
//		default:
//			printf("选择错误,请重新选择\n");
//			break;
//		}
//	} while (input);
//	return 0;
//}

//数组传参
//形参可以是数组，也可以是指针（数组指针）

//实参是函数的地址
//形参只能是函数指针